const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  getAllUsers() {
    const userArr = UserRepository.getAll();
    userArr.length > 0 ? userArr : null;
  }
  findUser(search) {
    return UserRepository.getOne(search) || null;
  }
  createUser(data) {
    return UserRepository.create(data) || null;
  }
  updateUser(id, dataToUpdate) {
    return UserRepository.update(id, dataToUpdate) || null;
  }
  deleteUser(id) {
    return UserRepository.delete(id) || null;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
