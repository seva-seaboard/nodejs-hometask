const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  getAllFighters() {
    const fighterArr = FighterRepository.getAll();
    fighterArr.length > 0 ? fighterArr : null;
  }
  findFighter(search) {
    return FighterRepository.getOne(search) || null;
  }
  createFighter(data) {
    return FighterRepository.create(data) || null;
  }
  updateFighter(id, dataToUpdate) {
    return FighterRepository.update(id, dataToUpdate) || null;
  }
  deleteFighter(id) {
    return FighterRepository.delete(id) || null;
  }
}

module.exports = new FighterService();
